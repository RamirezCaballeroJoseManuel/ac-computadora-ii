# Computadoras Electrónicas

``` plantuml
@startmindmap 
<style>
mindmapDiagram {
BackgroundColor white
    node {
        BackgroundColor white
        LineColor green
    }

    boxless {
    FontColor navy
    }

   arrow {
    LineThickness 2
    LineColor black
   }
    


}
</style>

   header
**Instituto Tecnologico de Oaxaca**
endheader
   caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**

* Computadoras Electrónicas

 *_ electromecánica
  * Harvard Mark I
   *_ origen
    * Creada por IBM en 1944
   *_ características
    * 765 mil componentes
    * 80 km de cable
    * Eje de 15 metros para mantener en sincronía el dispositivo
    * Motor de 15 HP
    * 3500 Relés
     * Interruptor mecánico controlado eléctricamente
      *_ compuesto por
       * Una bobina
       * Un brazo de hierro móvil
       * Dos contactos
      * Fallaba un Relé al día estadísticamente
   *_ uso
    * Simulaciones para el proyecto Manhattan
     *_ consistía en
      * Desarrollar una bomba atómica
   *_ realizaba
    * Tres sumas/restas por segundo
    * Multiplicación en 6 segundos
    * División en 15 segundos
    * Operaciones complejas en minutos

 * Colossus Mark I
  * Primera computadora electrónica programable
   * Su primera versión tenía 1600 tubos de vacío
    * Tenian la misma funcionalidad que los Relés, pero con mayor velocidad
   *_ configuración programable
    * Se realizaba mediante la conexion de cientos de cables en un tablero
  *_ origen
   * Diseñado por el Ingeriero Tommy Flowers en 1943. \nInstalada en el Bletchley Park (Reino Unido)
  *_ uso
   * Ayudaba a decodificar las comunicaciones Nazis
    * Se construyeron 10 Colossus para esta tarea

 * ENIAC
  * Primera computadora electronica programable de proposito general
   * Opero durante más de 10 años
   * Se calcula que hizo mas operaciones aritmeticas que toda la humanidad en dicho tiempo
  *_ origen
   * Diseñada por John Mauchly y J. Presper Eckert \nen la Universidad de Pensilvania en 1946
  *_ siglas de
   * Electrónica, Numérica, Integradora & Calculadora
  
  * Podía realizar 5 mil sumas/restas de 10 dígitos por segundo

 * IBM 608
  * Primera computadora disponible comercialmente basada en transistores
   * Transistor
    *_ es
     * Un interruptor eléctrico
      * Están hecho de silicio dopado
    *_ origen
     * Creado por los cientificos John Bardeen, Walter Brattain y William Shockley \ndentro de Bell Laboratories en 1947
    *_ características
     * Cumple la misma función que un relé o un tubo de vacío
     * Más sólidos y pequeños
    * Gracias a estos transistores se pudo construir esta computadora
  *_ origen
   * Lanzada por IBM en 1957
  *_ realizaba
   * 4500 sumas por segundo
   * 80 divisiones o multiplicaciones por segundo 

@endmindmap
```

# Arquitectura Von Neumann y Arquitectura Harvard
``` plantuml
@startmindmap
header
**Instituto Tecnologico de Oaxaca**
endheader
<style>
mindmapDiagram {
BackgroundColor cornflowerblue
    node {
        BackgroundColor white
        LineColor blue
    }

    boxless {
    FontColor navy
    }

   arrow {
    LineThickness 2
    LineColor black
   }

}
</style>
   caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**
  
* Arquitectura Von Neumann y Arquitectura Harvard
 * Ley de Moore
  *_ **establece que**
   * La velocidad del procesador total de las \ncomputadoras se duplica cada doce meses
    * Electrónica
     * El numero de transistores por \nchip se duplica cada año
     * El costo del chip permanece sin cambios
     * Cada 18 meses se duplica la potencia \nde calculo sin modificar el costo
    * Performance
     * Se incrementa la velocidad del procesador
     * Se incrementa la capacidad de la memoria
     * La velocidad de la memoria corre siempre \npor detras de la velocidad del procesador

 * Funcionamiento de una computadora
  *_ **antes**
   * Eran sistemas cableados y se \nprogramaba mediante hardware
    *_ **tenían**
     * Una entrada de datos
      *_ **>**
       * Secuencia de funciones aritméticas
        *_ **>**
         * Daba un resultado
  *_ **ahora**
   * Programación mediante software
    *_ **tenían**
     * Una entrada de datos
      *_ **>**
       * Secuencia de funciones aritméticas
        *_ **>**
         * Daba un resultado
        *_ **<**
         * Señales de control **<** Interprete de Instrucciones
          * Sirve para decir que secuencia de funciones \naritmeticas se van a ejecutar

 * Arquitectura Von Neumann
  *_ **es la** 
   * Arquitectura moderna
    *_ **modelo**
     * Descrita en 1945 por el matematico \ny fisico John Von Neumann
      * Los datos y programas se almacenan en \nuna misma memoria de lectura-escritura
      * Los contenidos de esta memoria se acceden \nindicando su posicion sin importar su tipo
      * Ejecución en secuencia
      * Representación binaria
      *_ **contiene**
       * Unidad Central de Procesamiento (CPU)
        *_ **contiene**
         * Unidad de Control
         * Unidad Aritmética Lógica
         * Registros
       * Memoria principal
        *_ **almacena**
         * Instrucciones y datos
       * Sistema de E/S
      * Tiene un sistema de Buses que \nse encuentra en la placa madre
       *_ **se divide en**
        * Bus de control
        * Bus de direcciones
        * Bus de datos e instrucciones
    * Surge el concepto de programa almacenado
    * La separación de la memoria y la CPU acarreo un problema
     *_ **se debe a**
      * La cantidad de datos que pasa entre estos dos elementos \ndifiere mucho en tiempo con las velocidades de ellos (Throughput)
    * Modelo de Bus
     *_ **definición**
      * El bus es un dispositivo en comun entre dos o mas dispositivos 
       * Si dos dispositivos transmiten al mismo tiempo señales, estas \npueden distorsionarse y consecuentemente perder informacion
        * Es por eso que exite un arbitraje para decidir quien hace uso del bus
     * Cada linea puede transmitir señales que \nrepresentan unos y ceros, en secuencia
     * Existen varios tipos de buses que realizan la tarea \nde interconexion entre las partes del computador
    *_ **propósito**
     * Reducir la cantidad de conexiones entre la CPU y sus sistemas 
    *_ **instrucciones**
     * La funcion de una computadora es la ejecucion de programas
      * La CPU se encarga de ejecutar dichas \ninstrucciones a traves de un ciclo de instruccion
       *_ **se emplea**
        * Lenguajes de bajo nivel y alto nivel
    * Ciclo de ejecución
     * Fetch de instrucción
     * Obtiene la proxima instruccion de memoria \ndejando la informacion en el registro de instruccion
     * Se incrementa el PC
     * La instrucción es decodificada
     * Calcular operando y fetch de operandos
     * Ejecución de instrucción y lectura de operandos

 * Arquitectura Harvard
  *_ **es la** 
   * Arquitectura Microcontroladores
    *_ **modelo**
     * Originalmente se referia a las Arquitecturas de Computadoras que utilizaba \ndispositivos de almacenamiento separados para las instruccion y datos
      *_ **contiene**
       * Unidad Central de Procesamiento (CPU)
        *_ **contiene**
         * Unidad de Control
         * Unidad Aritmética Lógica
         * Registros
       * Memoria
        *_ **se divide en**
         * Memoria de Instrucciones
          *_ **almacena**
           * Instrucciones del programa que debe ejecutar el microcontrolador \n(ROM, PROM, EPROM, EEPROM)
         * Memoria de Datos
          *_ **almacena**
           * Datos utilizados por el programa (RAM)
        *_ **solución Harvard** 
         * Fabricar memorias mucho mas rapidas tienen un precio muy alto \npor lo tanto las instrucciones y datos se almacenan en caches separadas para mejorar el rendimiento
          *_ **se utiliza**
           * En PICs o \nMicrocontroladores habitualmente en Electrodomesticos
       * Sistema de E/S
       * El sistema de buses se divide en dos para cada una de las memorias

@endmindmap
```

# Basura Electrónica
``` plantuml
@startmindmap
<style>
mindmapDiagram {
BackgroundColor white
    node {
        BackgroundColor white
        LineColor red
    }

    boxless {
    FontColor navy
    }

   arrow {
    LineThickness 2
    LineColor black
   }

}
</style>
header
**Instituto Tecnologico de Oaxaca**
endheader
caption **ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB**
* Basura Electrónica
 * Es chatarra electronica, artefactos electricos en desuso que son votados
  * Casi todos los circuitos contienen metales
   * Oro
   * Plata
   * Cobalto
   * Cobre
  * RAEE
   * Residuos de Aparatos Eléctricos y Electrónicos
    *_ Como se trata
     * Se selecciona según su categoría
     * Se desarman para recuperar los metales/plásticos/vidrios
     * Segregación y limpieza de las piezas
     * Se reciclan a nuevos productos o se exportan
    * Si se reciclan estos materiales \nprincipales se produce menos C02
  * La mayoria de basura electronica \nno es reciclada de manera segura
   * Existen centros de reciclaje que se encargan de \nreciclar basura electronica como lo es e-end
   * Algunos gobiernos estan tratando \nde solucionar este problema
    * La Union europea implemento una ley en 2016 que requiere que cada \npais recoja 45 toneladas por cada 100 toneladas puestas a la venta
   *_ consecuencias
    * Se exporta a paises de tercer \nmundo en un gran contenedor
     * Al ser desembalada con metodos primitivos \nprovoca un daño al medio ambiente
    * En ocasiones los discos duros tirados \npueden ser reparados y asi robarte informacion
  * En Mexico anualmente se desechan \n180 mil toneladas de basura electronica
   * En diversos lugares de Mexico existen \n "cementerios" de basura electronica
    * Tesoros electronicos
     * Se pueden encontrar diversos aparatos \nque fueron votados y muchos de ellos tienen reparación

@endmindmap
```
